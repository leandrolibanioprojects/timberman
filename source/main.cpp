#ifndef MAIN_CPP_FILE
#define MAIN_CPP_FILE

#include<sstream>
#include<SFML/Graphics.hpp>
#include<SFML/Audio.hpp>

const int NUM_BRANCHES = 6;

sf::Sprite branches[NUM_BRANCHES];

void updateBranches(int seed);

//Where is the player/branch
enum class sides{LEFT, RIGHT, NONE};
sides branchPositions[NUM_BRANCHES];

int main()
{
    //Create VideoMode object
    sf::VideoMode vm(1280, 720);

    //Create window
    sf::RenderWindow window(vm, "Timber Man!");

    //Set view for lower resolution support
    sf::View view(sf::FloatRect(0, 0, 1920, 1080));

    //Set view to window
    window.setView(view);


    //Create the Background texture
    sf::Texture textureBG;
    textureBG.loadFromFile("graphics/background.png");

    //Create the background sprite
    sf::Sprite spriteBG;
    spriteBG.setTexture(textureBG);
    spriteBG.setPosition(0, 0);

    //Create the tree Sprite and texture
    sf::Texture textureTree;
    textureTree.loadFromFile("graphics/tree.png");

    sf::Sprite spriteTree;
    spriteTree.setTexture(textureTree);
    spriteTree.setPosition(810, 0);

    //Preparing the bee
    sf::Texture textureBee;
    textureBee.loadFromFile("graphics/bee.png");

    sf::Sprite spriteBee;
    spriteBee.setTexture(textureBee);
    spriteBee.setPosition(0,800);
    //Is the bee currently moving?
    bool beeActive = false;
    //How fast can the bee fly
    float beeSpeed = 0.0f;

    //Making three clouds from 1 texture
    sf::Texture textureCloud;
    textureCloud.loadFromFile("graphics/cloud.png");

    sf::Sprite spriteCloud1;
    sf::Sprite spriteCloud2;
    sf::Sprite spriteCloud3;
    spriteCloud1.setTexture(textureCloud);
    spriteCloud2.setTexture(textureCloud);
    spriteCloud3.setTexture(textureCloud);

    spriteCloud1.setPosition(0, 0);
    spriteCloud2.setPosition(0, 250);
    spriteCloud3.setPosition(0, 500);
    //Are the clouds currently on screen
    bool cloud1Active = false, cloud2Active = false, cloud3Active = false;
    //How fast is each cloud
    float cloud1Speed = 0.0f;
    float cloud2Speed = 0.0f;
    float cloud3Speed = 0.0f;

    sf::Clock clock;

    //Time bar
    sf::RectangleShape timeBar;
    float timeBarStartWidth = 400.0f;
    float timeBarHeight = 80.0f;
    timeBar.setSize(sf::Vector2f(timeBarStartWidth, timeBarHeight));
    timeBar.setFillColor(sf::Color::Red);
    timeBar.setPosition(1920 / 2.0f - timeBarStartWidth / 2.0f, 980);

    sf::Time gameTimeTotal;
    float timeRemaining = 6.0f;
    float timeBarWidthPerSecond = timeBarStartWidth / timeRemaining;

    //Track whether the game is running
    bool paused = true;

    //Draw some text
    int score = 0;

    sf::Text messageText;
    sf::Text scoreText;

    //Choosing a font
    sf::Font font;
    font.loadFromFile("fonts/KOMIKAP_.ttf");

    //Set the font
    messageText.setFont(font);
    scoreText.setFont(font);

    //Assign the actual text
    messageText.setString("Press Enter to start!");
    scoreText.setString("Score = 0");

    //Set size
    messageText.setCharacterSize(75);
    scoreText.setCharacterSize(100);

    //Choose a color
    messageText.setColor(sf::Color::White);
    scoreText.setColor(sf::Color::White);

    //Position the text
    sf::FloatRect textRect = messageText.getLocalBounds();
    messageText.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
    messageText.setPosition(1920 / 2.0f, 1080 / 2.0f);

    scoreText.setPosition(20, 20);

    //Prepare 6 branches
    sf::Texture textureBranch;
    textureBranch.loadFromFile("graphics/branch.png");
    //Set texture for each branch sprite
    for(int i = 0; i < NUM_BRANCHES; i++)
    {
        branches[i].setTexture(textureBranch);
        branches[i].setPosition(-2000, -2000);
        //Set sprite origin to dead center
        branches[i].setOrigin(220, 20);
    }

    //Prepare the player
    sf::Texture texturePlayer;
    texturePlayer.loadFromFile("graphics/player.png");
    sf::Sprite spritePlayer;
    spritePlayer.setTexture(texturePlayer);
    spritePlayer.setPosition(580, 720);

    //The player starts on the left
    sides playerSide = sides::LEFT;

    //Prepare the gravestone
    sf::Texture textureRIP;
    textureRIP.loadFromFile("graphics/rip.png");
    sf::Sprite spriteRIP;
    spriteRIP.setTexture(textureRIP);
    spriteRIP.setPosition(600, 860);

    //Prepare the axe
    sf::Texture textureAxe;
    textureAxe.loadFromFile("graphics/axe.png");
    sf::Sprite spriteAxe;
    spriteAxe.setTexture(textureAxe);
    spriteAxe.setPosition(700, 830);

    //Line the axe up with the tree
    const float AXE_POSITION_LEFT = 700;
    const float AXE_POSITION_RIGHT = 1075;

    //Prepare the flying log
    sf::Texture textureLog;
    textureLog.loadFromFile("graphics/log.png");
    sf::Sprite spriteLog;
    spriteLog.setTexture(textureLog);
    spriteLog.setPosition(810, 720);

    //Some other useful log related variables
    bool logActive = false;
    float logSpeedX = 1000;
    float logSpeedY = -1500;

    bool acceptInput = false;

    //Prepare sounds
    sf::SoundBuffer chopBuffer;
    chopBuffer.loadFromFile("sound/chop.wav");
    sf::Sound chop;
    chop.setBuffer(chopBuffer);

    sf::SoundBuffer deathBuffer;
    deathBuffer.loadFromFile("sound/death.wav");
    sf::Sound death;
    death.setBuffer(deathBuffer);

    sf::SoundBuffer ootBuffer;
    ootBuffer.loadFromFile("sound/out_of_time.wav");
    sf::Sound oot;
    oot.setBuffer(ootBuffer);

    while(window.isOpen())
    {
        /*
        ****************************************************
        HANDLE PLAYER INPUT
        ****************************************************
        */
        sf::Event event;
        while(window.pollEvent(event))
        {
            if(event.type == sf::Event::KeyReleased && !paused)
            {
                //Listen for key presses again
                acceptInput = true;

                //hide the axe
                spriteAxe.setPosition(2000, spriteAxe.getPosition().y);
            }
        }
        //To close the game
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        {
            window.close();
        }
        //To pause/resume the game
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
        {
            paused = false;
            //Reset time and score
            score = 0;
            timeRemaining = 6.0f;

            //Make all branches disappear
            for(int i = 1; i < NUM_BRANCHES; i++)
            {
                branchPositions[i] = sides::NONE;
            }

            //Make sure the gravestone is hidden
            spriteRIP.setPosition(675, 2000);

            //Move the player into position
            spritePlayer.setPosition(580, 720);

            acceptInput = true;
        }

        if(acceptInput)
        {
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            {
                playerSide = sides::RIGHT;
                score++;

                //Add the amount of time remaining
                timeRemaining += (2/score) + .15;

                spriteAxe.setPosition(AXE_POSITION_RIGHT, spriteAxe.getPosition().y);
                spritePlayer.setPosition(1200, 720);

                //Update the branches
                updateBranches(score);

                //Set the log flying to the left
                spriteLog.setPosition(810, 720);
                logSpeedX = -5000;
                logActive = true;

                acceptInput = false;

                //Play sound
                chop.play();
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            {
                playerSide = sides::LEFT;
                score++;

                //Add the amount of time remaining
                timeRemaining += (2/score) + .15;

                spriteAxe.setPosition(AXE_POSITION_LEFT, spriteAxe.getPosition().y);
                spritePlayer.setPosition(580, 720);

                //Update the branches
                updateBranches(score);

                //Set the log flying to the left
                spriteLog.setPosition(810, 720);
                logSpeedX = 5000;
                logActive = true;

                acceptInput = false;

                //Play sound
                chop.play();
            }
        }
        /*
        ****************************************************
        UPDATE THE SCENE
        ****************************************************
        */
        if(!paused)
        {
            sf::Time dt = clock.restart();

            //Subtract from the amount of time remaining
            timeRemaining -= dt.asSeconds();
            //Size up the time bar
            timeBar.setSize(sf::Vector2f(timeBarWidthPerSecond * timeRemaining, timeBarHeight));

            if(timeRemaining <= 0.0f)
            {

                //Play sound
                oot.play();
                //Pause the game
                paused = true;

                //Change the message
                messageText.setString("Out of time!");

                //Resposition the text
                sf::FloatRect textRect = messageText.getLocalBounds();
                messageText.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
                messageText.setPosition(1920 / 2.0f, 1080 / 2.0f);
            }

            //Setup the bee
            if(!beeActive)
            {
                //How fast is the bee
                srand((int)time(0));
                beeSpeed = (rand() % 200) + 200;

                //How high is the bee
                srand((int)time(0) * 10);
                float height = (rand() % 500) + 500;
                spriteBee.setPosition(2000, height);
                beeActive = true;
            }
            else
            {
                //Move the bee
                spriteBee.setPosition(spriteBee.getPosition().x - (beeSpeed * dt.asSeconds()), spriteBee.getPosition().y);
                if(spriteBee.getPosition().x < -100)
                {
                    beeActive = false;
                }
            }

            //Manage the clouds
            //Cloud1
            if(!cloud1Active)
            {
                //How fast is the cloud
                srand((int)time(0) * 10);
                cloud1Speed = (rand() % 200);

                //How high is the cloud
                srand((int)time(0) * 10);
                float height = (rand() % 150);
                spriteCloud1.setPosition(-200, height);
                cloud1Active = true;
            }
            else
            {
                spriteCloud1.setPosition(spriteCloud1.getPosition().x + (cloud1Speed * dt.asSeconds()), spriteCloud1.getPosition().y);
                if(spriteCloud2.getPosition().x > 1920)
                {
                    cloud1Active = false;
                }
            }

            //Cloud2
            if(!cloud2Active)
            {
                //How fast is the cloud
                srand((int)time(0) * 20);
                cloud2Speed = (rand() % 200);

                //How high is the cloud
                srand((int)time(0) * 20);
                float height = (rand() % 300) - 150;
                spriteCloud2.setPosition(-200, height);
                cloud2Active = true;
            }
            else
            {
                spriteCloud2.setPosition(spriteCloud2.getPosition().x + (cloud2Speed * dt.asSeconds()), spriteCloud2.getPosition().y);
                if(spriteCloud2.getPosition().x > 1920)
                {
                    cloud2Active = false;
                }
            }

            //Cloud3
            if(!cloud3Active)
            {
                //How fast is the cloud
                srand((int)time(0) * 30);
                cloud3Speed = (rand() % 200);

                //How high is the cloud
                srand((int)time(0) * 30);
                float height = (rand() % 450) - 150;
                spriteCloud3.setPosition(-200, height);
                cloud3Active = true;
            }
            else
            {
                spriteCloud3.setPosition(spriteCloud3.getPosition().x + (cloud3Speed * dt.asSeconds()), spriteCloud3.getPosition().y);
                if(spriteCloud3.getPosition().x > 1920)
                {
                    cloud3Active = false;
                }
            }

            //Update the branch sprites
            for(int i = 0; i < NUM_BRANCHES; i++)
            {
                float height = i * 150;
                if(branchPositions[i] == sides::LEFT)
                {
                    //Move the sprite to the left side
                    branches[i].setPosition(610, height);
                    //Rotate the sprite
                    branches[i].setRotation(180);
                }
                else if(branchPositions[i] == sides::RIGHT)
                {
                    //Move the sprite to the right side
                    branches[i].setPosition(1330, height);
                    //Rotate
                    branches[i].setRotation(0);
                }
                else
                {
                    //Hide
                    branches[i].setPosition(3000, height);
                }
            }
            //Handle a flying log
            if(logActive)
            {
                spriteLog.setPosition(spriteLog.getPosition().x + (logSpeedX * dt.asSeconds()),
                    spriteLog.getPosition().y + (logSpeedY * dt.asSeconds()));

                if(spriteLog.getPosition().x < -100 || spriteLog.getPosition().x > 2000)
                {
                    logActive = false;
                    spriteLog.setPosition(810, 720);
                }
            }
            //Has the player been squished?
            if(branchPositions[5] == playerSide)
            {
                //death
                paused = true;
                acceptInput = false;

                //Draw the gravestone
                spriteRIP.setPosition(525, 760);

                //hide the player
                spritePlayer.setPosition(2000, 660);

                //Chage the message text
                messageText.setString("SQUISHED!");

                //Center it to screen
                sf::FloatRect textRect = messageText.getLocalBounds();
                messageText.setOrigin(textRect.left + textRect.width / 2.0f, textRect.top + textRect.height / 2.0f);
                messageText.setPosition(1920 / 2.0f, 1080 / 2.0f);

                //Play sound
                death.play();
            }
        }
        //Update the score text
        std::stringstream ss;
        ss << "Score = " << score;
        scoreText.setString(ss.str());

        /*
        ****************************************************
        DRAW THE SCENE
        ****************************************************
        */

        //Clear everything from the last frame
        window.clear();
        //Draw the game scene here:

        //Draw the background
        window.draw(spriteBG);
        //Draw the clouds
        window.draw(spriteCloud1);
        window.draw(spriteCloud2);
        window.draw(spriteCloud3);
        //Draw the branches
        for(int i = 0; i < NUM_BRANCHES; i++)
        {
            window.draw(branches[i]);
        }
        //Draw the tree
        window.draw(spriteTree);
        //Draw the player
        window.draw(spritePlayer);
        //Draw the axe
        window.draw(spriteAxe);
        //Draw the plying log
        window.draw(spriteLog);
        //Draw the gravestone
        window.draw(spriteRIP);
        //Draw the bee
        window.draw(spriteBee);

        //Draw the score
        window.draw(scoreText);

        //Draw time bar
        window.draw(timeBar);

        if(paused)
        {
            window.draw(messageText);
        }
        //Show everything we just drew
        window.display();
    }
    return 0;
}

void updateBranches(int seed)
{
    //Move all the branches down one place
    for(int j = NUM_BRANCHES - 1; j > 0; j--)
    {
        branchPositions[j] = branchPositions[j - 1];
    }

    //Spawn a new branch at position 0
    //LEFT, RIGHT or NONE
    srand((int)time(0)+seed);
    int r = rand() % 5;
    switch(r)
    {
        case 0:
            branchPositions[0] = sides::LEFT;
            break;
        case 1:
            branchPositions[0] = sides::RIGHT;
            break;
        default:
            branchPositions[0] = sides::NONE;
            break;
    }
}

#endif
