# TimberMan

TimberMan is a SFML test game. It was developed following the "Beginning C++ Game Programming" by PacktPub.

Actually, the game is not an OOP implemented project, but it is being updated constantly.

I'm providing a Code::Blocks project, but you should be able to manage it i any IDE you want;

Just make sure you have SFML latest version installed;

On Debian-Based linux:
	$ sudo apt update
	$ sudo apt install lisfml-dev make g++

On Windows:
	Install SFML to your minGW folder